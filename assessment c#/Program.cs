﻿using System;
using System.Collections.Generic;
using System.Linq;

class Developer
{
    public int Id;
    public string DeveloperName;
    public DateTime JoiningDate;
    public string ProjectAssigned;
    public bool OnContract;
    public int Duration;
    public double ChargesPerDay;
    public bool OnPayroll;
    public string Dept;
    public string Manager;
    public double NetSalary;
    public int Experience;

    // Salary components
    private double Basic;
    private double DA;
    private double HRA;
    private double LTA;
    private double PF;

    public Developer(int id, string name, DateTime joiningDate, string projectAssigned,
                     bool onContract, int duration, double chargesPerDay,
                     bool onPayroll, string dept, string manager, int experience)
    {
        Id = id;
        DeveloperName = name;
        JoiningDate = joiningDate;
        ProjectAssigned = projectAssigned;
        OnContract = onContract;
        Duration = duration;
        ChargesPerDay = chargesPerDay;
        OnPayroll = onPayroll;
        Dept = dept;
        Manager = manager;
        Experience = experience;
    }

    public void CalculateSalary()
    {
        Basic = ChargesPerDay * Duration;

        if (Experience >= 5)
            DA = Basic * 0.4;
        else
            DA = Basic * 0.3;

        HRA = Basic * 0.25;
        LTA = Basic * 0.1;
        PF = Basic * 0.12;

        NetSalary = Basic + DA + HRA + LTA - PF;
    }

    public double CalculateCharges()
    {
        if (OnContract)
            return ChargesPerDay * Duration;

        return 0;
    }

    public override string ToString()
    {
        return $"ID: {Id}\n" +
               $"Name: {DeveloperName}\n" +
               $"Joining Date: {JoiningDate}\n" +
               $"Project Assigned: {ProjectAssigned}\n" +
               $"On Contract: {(OnContract ? "Yes" : "No")}\n" +
               $"Duration: {Duration} days\n" +
               $"Charges Per Day: {ChargesPerDay}\n" +
               $"On Payroll: {(OnPayroll ? "Yes" : "No")}\n" +
               $"Department: {Dept}\n" +
               $"Manager: {Manager}\n" +
               $"Net Salary: {NetSalary}\n" +
               $"Experience: {Experience} years";
    }
}

class Program
{
    static List<Developer> developers = new List<Developer>();

    static void Main()
    {
        int choice;
        do
        {
            Console.WriteLine("\nMenu:");
            Console.WriteLine("1. Create Developer");
            Console.WriteLine("2. Calculate Salary");
            Console.WriteLine("3. Calculate Total Charges");
            Console.WriteLine("4. Display All Developers");
            Console.WriteLine("5. Display Developers with Net Salary > 20000");
            Console.WriteLine("6. Display Developers with 'D' in Name");
            Console.WriteLine("0. Exit");
            Console.Write("Enter your choice: ");
            choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    CreateDeveloper();
                    break;
                case 2:
                    CalculateTotalSalary();
                    break;
                case 3:
                    CalculateTotalCharges();
                    break;
                case 4:
                    DisplayAllDevelopers();
                    break;
                case 5:
                    DisplayDevelopersWithHighSalary();
                    break;
                case 6:
                    DisplayDevelopersWithNameContainingD();
                    break;
                case 0:
                    Console.WriteLine("Exiting the program...");
                    break;
                default:
                    Console.WriteLine("Invalid choice. Please try again.");
                    break;
            }
        } while (choice != 0);
    }

    static void CreateDeveloper()
    {
        try
        {
            Console.WriteLine("\nEnter Developer Details:");
            Console.Write("ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.Write("Name: ");
            string name = Console.ReadLine();

            Console.Write("Joining Date (yyyy-mm-dd): ");
            DateTime joiningDate = Convert.ToDateTime(Console.ReadLine());

            Console.Write("Project Assigned: ");
            string projectAssigned = Console.ReadLine();

            Console.Write("On Contract (true/false): ");
            bool onContract = Convert.ToBoolean(Console.ReadLine());

            int duration = 0;
            double chargesPerDay = 0;
            if (onContract)
            {
                Console.Write("Duration (in days): ");
                duration = Convert.ToInt32(Console.ReadLine());

                Console.Write("Charges Per Day: ");
                chargesPerDay = Convert.ToDouble(Console.ReadLine());
            }

            Console.Write("On Payroll (true/false): ");
            bool onPayroll = Convert.ToBoolean(Console.ReadLine());

            Console.Write("Department: ");
            string dept = Console.ReadLine();

            Console.Write("Manager: ");
            string manager = Console.ReadLine();

            Console.Write("Experience (in years): ");
            int experience = Convert.ToInt32(Console.ReadLine());

            Developer developer = new Developer(id, name, joiningDate, projectAssigned, onContract,
                                                duration, chargesPerDay, onPayroll, dept, manager, experience);

            developers.Add(developer);
            Console.WriteLine("Developer created successfully.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }

    static void CalculateTotalSalary()
    {
        try
        {
            foreach (Developer developer in developers)
            {
                developer.CalculateSalary();
                Console.WriteLine($"Salary calculated for Developer {developer.Id}: {developer.NetSalary}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }

    static void CalculateTotalCharges()
    {
        try
        {
            double totalCharges = 0;
            foreach (Developer developer in developers)
            {
                totalCharges += developer.CalculateCharges();
            }
            Console.WriteLine($"Total Charges: {totalCharges}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }

    static void DisplayAllDevelopers()
    {
        Console.WriteLine("\nAll Developers:");
        foreach (Developer developer in developers)
        {
            Console.WriteLine(developer);
            Console.WriteLine("--------------------");
        }
    }

    static void DisplayDevelopersWithHighSalary()
    {
        var highSalaryDevelopers = developers.Where(d => d.NetSalary > 20000);
        Console.WriteLine("\nDevelopers with Net Salary > 20000:");
        foreach (Developer developer in highSalaryDevelopers)
        {
            Console.WriteLine(developer);
            Console.WriteLine("--------------------");
        }
    }

    static void DisplayDevelopersWithNameContainingD()
    {
        var nameContainingDDevelopers = developers.Where(d => d.DeveloperName.Contains("D"));
        Console.WriteLine("\nDevelopers with Name containing 'D':");
        foreach (Developer developer in nameContainingDDevelopers)
        {
            Console.WriteLine(developer);
            Console.WriteLine("--------------------");
        }
    }
}